+++
title = "Getting Started With Bouldering"
date = "2022-05-11"
author = "Toyli"
description = "Some advice for people getting into bouldering"
tags = ["fitness", "bouldering"]
+++
> Bouldering: The sport of rock climbing on large boulders or low cliffs.

Bouldering is my favorite sport/workout for several reasons.
1. It's an easy way to get an engaging workout fast. Even with music and podcasts, treadmills and other exercises are boring. Bouldering makes you find routes while you climb, engaging your mind as well as your body.
2. Because the heights are usually 20 feet or less, bouldering also doesn't require any tall walls or ropes and harnesses to keep you safe and a crash pad at the bottom is typically good enough. This means little to no setup time is required and you can do it alone.
3. With no ropes to assist you, bouldering is also typically much more intense of a workout than traditional rock climbing. Not only does it hit your biceps, lats, rear deltoids, tricep long heads, and upper back muscles, it's also a great cardio workout if you build the endurance to do it for an extended period of time.

This post is going to be about indoor climbing gyms, as they're much easier for new climbers to get into.

## What Do You Need To Get Started?
Not much! The bare minimum is some fitting climbing shoes and most indoor bouldering gyms will rent you a pair for not very much. If you've gone a few times and decided bouldering is something you want to stick with, go to a local sporting goods store to find some fitting ones. I personally wear [Black Diamond Momentums](https://www.rei.com/product/190132/black-diamond-momentum-climbing-shoes-womens) I got at REI, but **find whatever fits you as snugly as possible without discomfort**. Some additional things you might consider are:
- Chalk and chalk bag/bucket: You can climb without chalk, but you're going to have a much better time with it. I use [FrictionLabs Unicorn Dust](https://www.rei.com/product/898349/frictionlabs-unicorn-dust-fine-chalk) because it works well for me and is made locally, but like all things finding what works for you will require trial and error. Some gyms require using [chalk balls](https://www.rei.com/product/102675/frictionlabs-magic-chalk-ball) to reduce chalk mess.
- A file or pumice stone for maintaining important calluses, allowing you to climb longer and keep your hands from getting torn up.
- Climbing tape: I haven't found a brand I like yet, but it's important to take care of your hands and wrapping your fingers will keep you from tearing them apart[^1].

## Problems
If you're going to an indoor gym, there will be walls with different colored holds. Each color represents a different "problem" of a certain difficulty. To climb the problem, you should only use holds of the same color.
### V Scale
There are two main grading scales for problem difficulties, the V Scale and the Font Scale. In the United States, we use the V Scale, while the Font scale is used throughout Europe and Asia. Because I'm American, I will be writing about the V Scale.

This is not a really scientific measuring system. On indoor problem, the routesetter (the person who designed the problem) will climb it once or twice and assign a grade between VB[^2] and V17. If the routesetter isn't sure what grade it should be, they will get opinions from other people who are shorter, taller, have different length arms, etc. and they will come to a consensus about the difficulty rating. For outdoor routes, boulders don't tend to move or change very much so climbers will generally come to a consensus on their own. Sometimes a + or - will be affixed to the difficulty to put it between numbers. For example, V8+ is harder than V8 but easier than V9.

## Climbing Technique

### Falling
You are going to fall. It's nothing to be ashamed of, everyone does it and nobody will judge you. What's important is falling with proper form. You're not very far up, but you can still get hurt. Falling safely takes practice, but here's some tips to keep in mind:
- Before you start climbing, make sure nothing is on the pads below you.
- Don't try to catch yourself on other holds. Just let it happen.
- Try to land on your feet first.
- Keep your knees somewhat relaxed and allow them to collapse, rolling onto your back or side. [Force follows stiffness](https://youtu.be/oBcxuzdP3rs?t=7m16s), and you don't want to absorb the entire force of your fall in your knees.
- Never land with your arms out or try to absorb the impact with your wrists. Evolution betrays us here, so if you're new to climbing it's a reflex you will have to fight.
- On big falls, tuck your chin to avoid whiplash.

### Climb With Your Legs
Your legs hold you up all day, but how long can you do a handstand? For most people, your legs are stronger than your arms. Try to have most of the strength you use while climbing come from your legs and guide yourself with your arms. Hanging from handholds will tear up the skin on your hands and wear you out faster, so keep your weight on your footholds whenever possible.

[^1]: It's important to remember that always wrapping your hands and fingers will reduce your grip strength and prevent you from strengthening the skin on your hands.
[^2]: The B stands for "beginner" or "basic"