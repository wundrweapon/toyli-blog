+++
title = "Error Type Erasure"
date = "2022-05-01"
author = "Toyli"
description = "The helpful error handling tool I forgot to mention"
tags = ["rust", "errorhandling", "programming"]
Toc = false
+++
In my two-part post about Rust's error handling and `Result` type, I talked about common mistakes people make when approaching Rust's somewhat unique approach to error handling, explained the tools that are available to you, and how to effectively use the `Result` type in your own code. But I forgot another way that some people do, boxed error trait objects.

Let's say you're in a situation where you can't handle errors within the function because you don't have enough context. You still need to get to the inner values, so your code will probably end up containing a lot of `unwrap` calls. For example, requesting a serialized object from a remote URL and then using a trait.
{{< code language="rust" title="Unwrapping some errors" expand="Show" collapse="Hide" isCollapsed="false" >}}
use reqwest::blocking::get;
use std::io;

struct SomeStruct {/* Whatever */}
impl SomeStruct {fn some_function() -> Result<String, io::Error> {/* Whatever */}}

fn some_other_function() {
    let object: SomeStruct = get("http://example.com/object.json")
        .unwrap() // Assume the request was successful
        .json() // Attempt to deserialize
        .unwrap() // Assume deserialization was successful
    let value: String = object
        .some_function() // Attempt to use the implemented trait
        .unwrap() // Assume it was successful
}
{{</code>}}

What you could do to handle these errors instead of blindly unwrapping is return the error values to where this function was called with the `?` operator. That higher level should have enough context better decide whether and how to try to handle the error. but this runs you into another problem in that `Result` can only have one error type, but this function has both `reqwest::Error` and `io::Error` types that can occur. The following will not compile because formatting errors cannot be converted to IO errors, but an IO error is required by the function signature.
{{< code language="rust" title="This Code Won't Compile" expand="Show" collapse="Hide" isCollapsed="false" >}}
fn type_one() -> Result<(), std::io::Error> { todo!(); }
fn type_two() -> Result<(), std::fmt::Error> { todo!(); }
fn both_types() -> Result<(), std::io::Error> {
  type_one()?;
  type_two()?;
  todo!();
}

fn main() { both_types(); }
{{</code>}}
```
error[E0277]: `?` couldn't convert the error to `std::io::Error`
 --> test.rs:6:13
  |
4 | fn both_types() -> Result<(), std::io::Error> {
  |                    -------------------------- expected `std::io::Error` because of this
5 |   type_one()?;
6 |   type_two()?;
  |             ^ the trait `From<std::fmt::Error>` is not implemented for `std::io::Error`
  |
  = note: the question mark operation (`?`) implicitly performs a conversion on the error value using the `From` trait

error: aborting due to previous error

For more information about this error, try `rustc --explain E0277`.

```

So how can we use the `?` operator to return any of the multiple types of error? Both format and IO (and almost all other error types in the Rust ecosystem) implement the `std::error::Error` trait. We can use the `dyn` keyword to make a [trait object](https://doc.rust-lang.org/std/keyword.dyn.html) for the Error trait, and either error type will satisfy the function signature. First, we need to change the function signature to take a trait object instead of a specific error kind:
```rust
fn both_types() -> Result<(), dyn std::error::Error> {}
```
This code, however, still won't compile. Result requires that the Error type implements the [Sized](https://doc.rust-lang.org/std/marker/trait.Sized.html) trait. This is part of Rust's memory safety guarantees and means the size of the type in memory needs to be known at compile-time. To resolve this problem, we need to `Box`[^1] the error.

Because `Box` is a pointer to the value instead of containing the value itself, it has a size known at compile-time and satisfies the requirements of the compiler. Because `Box` implements the [From](https://doc.rust-lang.org/std/boxed/struct.Box.html#impl-From%3CE%3E) trait for error types, the `?` operator will handle the boxing for us and this code below will compile with very minimal changes from the original.

{{< code language="rust" title="This Code Will Compile" expand="Show" collapse="Hide" isCollapsed="false" >}}
use std::boxed::Box;

fn type_one() -> Result<(), std::io::Error> { todo!(); }
fn type_two() -> Result<(), std::fmt::Error> { todo!(); }
fn both_types() -> Result<(), Box<dyn std::error::Error>> {
  type_one()?;
  type_two()?;
  todo!();
}

fn main() { both_types() }
{{</code>}}

[^1]: If you're coming to Rust from another language like C, "boxing" values is the simplest way we do heap allocation.