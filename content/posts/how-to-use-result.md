+++
title = "How To Use Result In Your Own Code"
date = "2022-04-01"
author = "Toyli"
description = "In the last post, we went over how to handle recoverable errors in Rust. Here's how to use Results effectively in your own code."
tags = ["rust", "errorhandling", "programming"]
+++
In the [last post](/posts/unwrapping-christmas-presents/), I explained the difference between recoverable and unrecoverable errors, how Rust differs from most programming languages, and how to leverage Rust's Result type to make your programs more robust. In this post, we will go over how to effectively use the Result type in your own code and return useful Results from your own functions.

## The Basic Model

Result is an enum. Like any enum, it's a bit like Schrödinger's cat. You can't really know what variant it is (Ok or Err) until you somehow inspect it. When you define a Result in a function signature, you have to provide two types, the success and error value. The requirements for an Err type are:
- It must have a size known at compile time
- It should be able to adapt to represent various kinds of errors that can happen in the function
- It should be easily comparable with a match statement to adapt to the error type
- It should ideally be self-documenting to some extent

## Don't Use Strings For Err
A common mistake new Rust users do when trying to make their own errors is to return them as Strings because they make the compiler happy by having a size known at compile time and can represent any error message. For example:

{{< code language="rust" title="Returning `String` as an error" expand="Show" collapse="Hide" isCollapsed="false" >}}
fn function() -> Result<(), String> {
    if (failure_condition) {
        return Err("A bad thing happened!".to_string());
    }
    if (other_failure_condition) {
        return Err("A different bad thing happened!".to_string());
    }
    Ok(())
}
{{</code>}}

However, this is a bad idea. Although this approach will satisfy the compiler and represent multiple error kinds, you cannot have a guarantee that you have exhaustively matched every String that is returned from your function. You may have forgotten some and the only way you'll find out is if they occur in reality.

## Don't Use Integers For Err
Another type that has a size known at compile time, can represent various values, and is easily comparable with a match statement is Rust's various integer primitives. Compared to Strings, if you iterate your error integers it will be easier to recognize that you missed one and you are protected against small typo differences in your error strings. For example:
{{< code language="rust" title="Missed one" expand="Show" collapse="Hide" isCollapsed="false" >}}
fn function() -> Result<(), u8> { /* ... */ }

fn main() {
    match function() {
        Ok(_) => { println!("Function ran"); }
        Err(0) => { /* ... */ },
        Err(1) => { /* ... */ },
        // 2 has been obviously forgotten
        Err(3) => { /* ... */ },
        Err(4) => { /* ... */ },
        _ => { /* ... */ }
    }
}
{{</code>}}

However, this approach is still not ideal. It is not even remotely self-documenting, requires accurate notes of what error each number corresponds to, additional values can be forgotten at the end of the list, and you cannot have a compiler guarantee that you have exhaustively handled all possible errors if you don't want to use a blanket `_`.

## Do Use An ErrorKind Enum
The correct way to have an error type that has a size known at compile time, represents all possible errors in your module, is easily comparable with match statements, and is human-understandable when reading the code is to make an enum. It can even contain extra information about errors from other modules, such as IO errors.

With this approach, no catch-all is needed at the end of the match statement. You can be confident that you have exhausted all possible errors (the compiler will fail otherwise). The enum variants, like Strings, are human-readable descriptions of the error and, like integers, are also immune to typos.
{{< code language="rust" title="Enum Variant Errors" expand="Show" collapse="Hide" isCollapsed="false" >}}
enum ErrorKind {
    BadThing,
    OtherBadThing,
    // Represents IO errors and contains additional data
    Io(std::io::Error)
}

fn function() -> Result<(), ErrorKind> { /* ... */ }

fn main() {
    use ErrorKind::*;
    match function() {
        Ok(_) => { println!("Function ran"); }
        Err(BadThing) => { /* ... */},
        Err(OtherBadThing) => { /* ... */ },
        Err(Io(e)) => match e { /* ... */ }
    }
}
{{</code>}}