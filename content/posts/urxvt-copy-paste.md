+++
title = "Copy and Paste with urxvt"
date = "2022-05-21"
author = "Toyli"
description = "urxvt supports copying and pasting now. You don't need a perl hack for it."
tags = ["linux"]
+++
If you look up a tutorial for how to configure urxvt, you might find one like [this](https://addy-dclxvi.github.io/post/configuring-urxvt/). It suggests using `xsel` and Muennich's [urxvt-perls](https://github.com/muennich/urxvt-perls) repository to handle copying and pasting to the clipboard in urxvt, but mentions:
> Some extensions marked as deprecated, but it still works for me without any issue.

There is a reason the extension is marked as deprecated. It's not necessary anymore. urxvt supports this on its own now. From the urxvt-perls repository:
> DEPRECATED
> 
> Since version 9.20 rxvt-unicode natively supports copying to and pasting from the CLIPBOARD buffer with the Ctrl-Meta-c and Ctrl-Meta-v key bindings. The clipboard.autocopy setting is provided by the selection-to-clipboard extension shipped with rxvt-unicode.

To copy and paste with Ctrl+Shift+C/V in urxvt, add this to your `~/.Xresources` file:
{{<code language="X" title="Add this to ~/.Xresources" expand="Show" collapse="Hide" isCollapsed="false">}}
!! URxvt copy/paste
URxvt.keysym.C-S-0x43: eval:selection_to_clipboard
URxvt.keysym.C-S-0x56: eval:paste_clipboard
{{</code>}}
