+++
title = "Save Unwrapping For The Christmas Presents"
date = "2022-03-31"
author = "Toyli"
description = "The Do's and Dont's of Error Handling in Rust"
tags = ["rust", "errorhandling", "programming"]
+++

## Meet Result, Your New Best Friend

Let's say you're new to Rust. You just made your `Hello, World!` and it printed to stdout and all is well in the world. It's time to take it a step further. How about, say, creating a file? You look at the docs for the `std::fs::File` struct and find `File::create`. Time to use it!

{{< code language="rust" title="Creating a file in Rust" id="1" expand="Show" collapse="Hide" isCollapsed="false" >}}
use std::fs::File;

fn main() {
    let file = File::create("testfile.txt");
}
{{</code>}}

Run the code and the file is created! Now time to try to write something to it.

{{< code language="rust" title="Writing to the file" expand="Show" collapse="Hide" isCollapsed="false" >}}
use std::fs::File;
use std::io::Write;

fn main() {
    let mut file = File::create("testfile.txt");
    file.write(b"Hello, World!");
}
{{</code>}}
But this time when you run the code, you get this error:
```
   Compiling rusttest v0.1.0 (/home/aayla/Documents/Code/rusttest)
error[E0599]: no method named `write` found for enum `Result` in the current scope
 --> src/main.rs:7:10
  |
7 |     file.write(b"Hello, world!");
  |          ^^^^^ method not found in `Result<File, std::io::Error>`
```
## Why Does This Happen?
This happens because `File::create()`, unlike Python's `open()` for example, does not return the file object. It returns a `Result<File, std::io::Error>`. Coming from another language, you might not understand what the Result type is for. This is because most programming languages do not distinguish between recoverable and unrecoverable errors.

To use Python (a language where all errors are treated as unrecoverable) as an example again, if you run `open("/", 'w')`, you will receive an `IsADirectoryError` and the entire program will crash or, in Rust speak, panic. To handle this, you could wrap that in a `try` statement and have a special case for `IsADirectoryError` and handle errors based on their type like the example below. The code will now print the error message instead of crashing, but unwrapping the stack with try statements is computationally expensive and _you have to remember to do it_.

When functions in Rust return a variant of the `Result` enum, _some_ form of error handling is required. Whether that be a simple `.unwrap()` or a full `match` tree, the program will only panic if you tell it to.
{{< code language="python" title="Python `except` example" expand="Show" collapse="Hide" isCollapsed="false" >}}
try:
    open("/", "w")
except IsADirectoryError:
    print("Provided path is a directory!")
except:
    print("Some unhandled error happened")
{{</code>}}

## So What Do I Do With It?
When a lot of new Rust developers encounter `Result`, the [unwrap method](https://doc.rust-lang.org/std/result/enum.Result.html#method.unwrap) is very appealing. It makes the compiler happy and gets you the value you want access to. Let's try to open the file we already made in the last example and try to write to it. This code will compile. And, if you run the code and `cat testfile.txt`, the content has been written!
{{< code language="rust" title="Writing to the file, but really this time" expand="Show" collapse="Hide" isCollapsed="false" >}}
use std::fs::File;
use std::io::Write;

fn main() {
    let mut file = File::open("testfile.txt")
        .unwrap(); // This gets us our File
    file.write(b"Hello, World!");
}
{{</code>}}
However, if you remove testfile.txt and try to run it again, there's a problem.

> thread 'main' panicked at 'called `Result::unwrap()` on an `Err` value: Os { code: 2, kind: NotFound, message: "No such file or directory" }', src/main.rs:6:47
> 
> note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace

This is where the title of this post comes in. Because of the `.unwrap()` call, this code treats any filesystem errors (in this case, `NotFound`) as unrecoverable. By using `.unwrap()`, you are ignoring one of the most important tools Rust has to offer you and telling the compiler to assume the code will always run successfully and panic if it doesn't. So how _should_ you use Results?

### `unwrap`
Every time you unwrap a Result, you are making an assumption that the function will always be successful and your program will panic if it's not. While unwrapping is usually not the approach you should take, that doesn't mean you should never do it. Sometimes you might have more information than the compiler. In this example, because "27" is a constant, parsing it as an integer will never fail.
{{< code language="rust" title="This Result will never be an error" expand="Show" collapse="Hide" isCollapsed="false" >}}
fn main() {
    let my_string = "27".to_string();
    let my_int = my_string.parse::<i32>().unwrap();
}
{{</code>}}

### `unwrap_or`
Not every failed function requires complex error handling. `unwrap_or` lets you either get the Ok value, or returns a default if the Result is an Error.
{{< code language="rust" title="Returning a default value with `unwrap_or`" expand="Show" collapse="Hide" isCollapsed="false" >}}
fn main() {
    let x: Result<u32, &str> = Ok(9);
    let y: Result<u32, &str> = Err("error");
    
    println!("{}", x.unwrap_or(5)); // This will print 9
    println!("{}", y.unwrap_or(5)); // This will print 5
}
{{</code>}}

### `unwrap_or_else`
Similar to `unwrap_or`, but instead of returning a constant default the default is computed from a provided closure.
{{< code language="rust" title="Computing a default value with `unwrap_or_else`" expand="Show" collapse="Hide" isCollapsed="false" >}}
fn main() {
    fn count(x: &str) -> usize { x.len() }

    let x: Result<usize, &str> = Ok(9);
    let y: Result<usize, &str> = Err("error");
    
    println!("{}", x.unwrap_or_else(count)); // This will print 9
    println!("{}", y.unwrap_or_else(count)); // This will print 5
}
{{</code>}}

### `expect`
Sometimes errors that are generically recoverable should be treated as unrecoverable for your specific program. For example, failing to initialize logging with [simple_logger](https://docs.rs/simple_logger/2.1.0/simple_logger/). This is an ideological position, but personally I believe that no program should attempt to continue if it cannot report its own state through logging, so while `SimpleLogger::init()` returns a Result, I am not interested in trying to recover.

While `unwrap` and `expect` both panic on error, `expect` is more explicit about your intentions (the entire program should fail if this function does instead of assuming the function will always succeed) and will panic more cleanly with a defined error message.
{{< code language="rust" title="Use `expect` to abort if a critical function returns an error" expand="Show" collapse="Hide" isCollapsed="false" >}}
use simple_logger::SimpleLogger;

fn main() {
    SimpleLogger::new()
        .env()
        // This will return Result
        .init()
        // If logging fails to initializing fails, abort with an error message.
        .expect("Failed to initialize logging");
    log::info!("Initializing logging was successful!");
}
{{</code>}}
### `match` on different errors
This is the approach you will be using most often if you want to make full use of Rust's Result. By using a `match` statement, you can engineer your program to execute different logic based on the kind of error that is returned. One of the key advantages of Rust's Result is that the error type is something you can work with easily, usually an enum, as opposed to parsing strings in some other languages. Especially with enum errors, this means you can easily and exhaustively handle specific errors differently.

In the following example, the program attempts to open testfile.txt. If this is successful, the file object is returned with no extra logic needed. If `File::open` fails, however, the error kind can be passed to a match statement to decide what to do.
- In the case of `ErrorKind::NotFound`, the error can be recovered from by creating the missing file and returning the file object. If the file creation fails, our attempt to recover from the error has failed and it's time to panic.
- In the case of `ErrorKind::PermissionDenied`, this is a problem with the environment the program is running in. In most cases, it is outside the scope of the program to try to correct problems with the environment it is running in, so we should panic instead.
- With the `_` option, all other cases that have not been explicitly matched will be handled by this branch. The Rust compiler will refuse to compile a `match` tree without exhaustive matching. This ensures that all possible errors that can be returned by a function will be handled somehow.

{{< code language="rust" title="Use `match` to decide what to do based on the error" expand="Show" collapse="Hide" isCollapsed="false" >}}
use std::fs::File;
use std::io::ErrorKind;

fn main() {
    let file = match File::open("testfile.txt") {
        // The file was opened successfully, so we can simply return the Ok value
        Ok(file) => file,
        // The result was an error, so determine what to do
        Err(error) => match error.kind() {
            // Here we try to recover from NotFound by creating the missing file
            ErrorKind::NotFound => match File::create("testfile.txt") {
                // Recovery succeeded, program can continue as if nothing happened
                Ok(file) => file,
                // Recovery failed, so we can panic now
                Err(e) => panic!("Error creating missing file {:?}", e),
            },
            // Don't attempt to fix the environment
            ErrorKind::PermissionDenied => { panic!("Permission Denied"); },
            // Match all other errors
            _ => { panic!("Unhandled Error"); }
        }
    }
}
{{</code>}}

In the next post, we will go over how you can utilize the Result type in your own code.