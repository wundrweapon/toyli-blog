+++
title = "There Is No Time"
date = "2022-04-11"
author = "Toyli"
description = "As programmers, a lot of things depend on time. We use it so regularly and languages have such robust implementations that often we forget that they're built on a mass hallucination."
tags = ["programming", "time"]
+++

## Computers Are Good At Duration
Computers are pretty good at measuring duration. So good, in fact, that [two atomic clocks were used to measure time dilation in real life](https://www.resonancescience.org/blog/time-dilation-experiment-with-atomic-clock-opens-possibility-to-measure-relativistic-effects-in-matter-in-quantum-state). In programming, we use time a lot. I recently added a feature to [raccoon.club](https://gitlab.com/toyli/raccoon.club/-/blob/f7d9a30924436025118fe5eb40b2cfd901a8e313/src/stats.rs#L58) where, if you request a page, there will be a header `x-response-time` with the time it took the server to process your request in milliseconds.
{{< code language="shell" title="Quering the headers of a test server" expand="Show" collapse="Hide" isCollapsed="false" >}}
$ curl -sD - -o /dev/null http://localhost:8000
HTTP/1.1 200 OK
content-type: text/html; charset=utf-8
server: Rocket
x-content-type-options: nosniff
permissions-policy: interest-cohort=()
x-frame-options: SAMEORIGIN
x-response-time: 1 ms
content-length: 58377
date: Mon, 11 Apr 2022 20:47:59 GMT
{{</code>}}
It seems obvious that computers should be good at keeping time. Your CPU has a certain clockspeed (usually measured in GHz these days). If a CPU knows that it runs at a clock of 1GHz and that it has gone through a billion cycles, that's a second(ish). This is a gross simplification and processors ship with monotonic clocks far more accurate than this approach, but that's besides the point. Programmers use durations a lot and computers are really quite good at them.

So why is it, then, if computers are so good at keeping track of duration, there's a famous [Computerphile video](https://www.youtube.com/watch?v=-5wpm-gesOY) of Tom Scott slowly losing his mind over time zones? That's because time is a mass hallucination with political rules, and no language makes that more inconveniently obvious than Rust.

## The Inspiration For This Post
My friend [@wundrweapon](https://gitlab.com/wundrweapon) recently had the idea to make a to-do list program, and decided to do it in Rust. I threw out the idea of adding due dates to the items, and pointed him to [`std::time::Instant`](https://doc.rust-lang.org/std/time/struct.Instant.html). It seemed simple, to make the due date just make an `Instant` for now and then add a `Duration` for how long until the task is due. He later asked me a seemingly very simple question:
> how do i just get the the time from std::time::Instant? unix ms, unix seconds, ISO-formatted string, idrc
> 
> \- @wundrweapon

I thought about it for a bit. Surely you can get an instant represented as some unit of time. After all, Python has `time.time()`, C++ has `std::time`, etc. So how do you get a Rust `Instant` as a measure of time? **You can't**.

What all of those languages are abstracting away while Rust makes you do it manually, is what seems like on the surface some pretty simple math. Those time functions are not representing that moment in time as ms for the same reason you can't measure a geometric point in inches. Seconds are a measure of duration and you need two points to make a line. To do what wundrweapon was trying to do, you have to do this:

```rust
use std::time::{Instant, UNIX_EPOCH};
let timestamp = Instant::now().duration_since(UNIX_EPOCH).unwrap().as_secs();
```

But, to do that subtraction, you have to have both numbers. Where do they come from?

## How Do Computers Keep Time?
Like I said at the beginning of this post, computers are really good at durations. What is the current epoch unix timestamp? While I'm writing this it's 1649717344. There's a battery somewhere in your computer that keeps the clock ticking even when it's unplugged from the wall, but my computer hasn't been running since January 1st, 1970. As far as I'm aware, no computer has. And what happens if that battery dies? The clock on most devices agrees, and that's too well coordinated for coincidence. Whose time is it anyways? Enter the Network Time Protocol.

In operation since 1985, NTP is one of the oldest internet protocols still in use and it's responsible for keeping all computers synchronized with universal standard time. If your computer doesn't know what the current time is, it can just ask another computer. But how does _any_ computer know what time it is? This is where engineering meets politics.

## Time Is A Mass Hallucination

Tom Scott lost his mind for 10 minutes and 12 seconds over time for the same reason wundrweapon ended up abandoning the idea of due dates on his to-do list program: **Time is not real**. Duration is real. It's not necessarily consistent and the units are somewhat arbitrary, but it's measurable, quantifiable, all that fun stuff but time is not.

Computers agree what time it is not because time can be measured, but because some government said that's what time it is. Maybe the European Union said so through Galileo, or maybe it was American NIST through [WWVB](https://en.wikipedia.org/wiki/WWVB) in Fort Collins, Colorado. Time is political, and political time has a _lot_ of rules. We know time to be more or less simple and intuitive because it's such an essential part of our everyday lives, it's been there the whole time, and there's enough clocks to give the illusion that there is no authority on time. In keeping with the conclusions of every other engineer who has approached time, though, it is a fever dream of laws, political agendas, and special cases that is best left alone.