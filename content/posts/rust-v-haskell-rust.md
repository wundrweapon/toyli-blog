+++
title = "Rust vs Haskell Binary Size - The Rust Perspective"
date = "2022-03-19"
author = "Toyli"
description = "I was challenged by @wundrweapon to make a Rust implementation of a 3 way sort Haskell program he wrote and see how small I could shrink the binary."
tags = ["rust", "programming"]
+++

Recently I was challenged by [@wundrweapon](https://gitlab.com/wundrweapon) to reimplement a 3 way sort [program](https://gitlab.com/wundrweapon/haskell-misc) he had made in Haskell in Rust to see which language could create a smaller binary. If you want the tl;dr, Haskell did win out in the end, but very narrowly. I managed to shrink the Rust binary down to 17KB and the Haskell binary came out to 15KB.

## The Requirements
The requirement of the program was that it reimplements the functionality of the Haskell program, trying to the best of my ability to minimize the side effects of using Rust. This means avoiding mutability whenever possible, cloning even when unnecessary, etc. This means the code I wrote, while not very rusty or efficient, is close to a mirror of what the Haskell program is doing not only in producing the same output, but also the same process under the hood. The full requirements were:

- Implement the same 3 special cases he did:
    - An argc of 0 should return an empty list
    - An argc 1 should return a clone of itself. The clone is required because of how Haskell works.
    - An argc of 2 should use an if statement to compare the two elements and either return the original input or a reversal of the input
- Avoid mutability wherever possible

## Writing the Program
Writing the program was a struggle. I had to fight against my usual practices when writing Rust code and make things significantly less efficient in some spots. To be completely honest, I don't know much about sorting algorithms. I don't have a formal computer science education so I'm lacking a lot of vocabulary and specific knowledge. wundrweapon had to walk me through implementing the sorting algorithm, and I was initially concerned about the recursion [here](https://gitlab.com/toyli/rust-size-test/-/blob/d2ed2ab8705b3ca8e72cc3d2ee150ed8f30f4b55/src/main.rs#L17), but he reassured me it was supposed to be like that. If there's one thing I learned from this project, it was a very good reminder of how much I have left to learn.
> 3 way sort is very efficient, but very unkind to your memory
> - wundrweapon

## Making It Small
A major help in learning different tricks for making the program smaller was [johnthagen/min-sized-rust](https://github.com/johnthagen/min-sized-rust). I definitely couldn't have competed in this challenge without it. Here's a step-by-step of how big the binary was.

|Step|Size|Percentage Reduction|
|---|---|---|
|Debug build|3.8M|N/A|
|Release build|3.5M|7.89%|
|Enable `strip`|307K|91.23%|
|Set `opt-level = "z"`|311K|-1.3%|
|Enable Link Time Optimization|275K|11.58%|
|Set `codegen-units = 1`|275K|0%|
|Abort On Panic|259K|5.82%|
|Optimize `libstd` with `build-std`|203K|21.62%|
|Remove `panic` String Formatting|35K|82.76%|
|Compress The Binary With `upx`|17K|51.43%|

As you can see, the most significant reductions were made by enabling binary stripping in cargo, removing panic string formatting, and compressing the binary with `upx`.

## Conclusions
I am confident that I could make the binary even smaller with additional steps outlined in the guide written by johnthagen, such as `#![no_std]` or removing the default formatter with `#![no_main]`, but at this point I had lost interest in trying to make it smaller and that would have required significant code changes.