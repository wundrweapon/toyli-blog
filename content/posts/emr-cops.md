+++
title = "EMR: A Made-Up Certification For Cops"
date = "2022-04-04"
author = "Toyli"
description = "Teaching you everything you need to know to get Emergency Medical Responder certified in a single blog post to prove a point."
tags = ["medicine"]
readingTime = true
+++
## What is EMR?
The National Registry of Emergency Medical Technicians has several levels of certification. Nationally Registered Emergency Medical Technician (NREMT), Nationally Registered Advanced Emergency Medical Technician (NRAEMT), and the most advanced level: Nationally Registered Paramedic (NRP). However, there is a fourth level of certification you may not know about: Nationally Registered Emergency Medical Responder. Required by most police departments, it really only exists so cops can claim to have medical training and a fancy certificate name that's only one word different from EMT.

When it comes to actual skills, EMR is pretty much worthless and to prove it I'm going to teach you everything you need to know to pass the test in this blog post. Does that word count at the top seem concerningly short for an emergency medical certification? It should.

## Getting Started
[The National EMS Scope of Practice Model](https://www.ems.gov/pdf/National_EMS_Scope_of_Practice_Model_2019.pdf) is broken up into 7 skills:
- Airway / Ventilation / Oxygenation
- Cardiovascular / Circulation
- Splinting, Spinal Motion Restriction (SMR), and Patient Restraint
- Medication Administration
- Medical Director Approved Medications
- IV Initiation/Maintenance Fluids
- Miscellaneous

## Airway / Ventilation / Oxygenation
### Basic Airway Management
When a person becomes unconscious, the muscles in their jaw relax and this can allow the tongue to obstruct the airway. This is bad because people need air to not die, which is where airway management comes in. The EMR certification requires knowledge of the head tilt / chin lift and jaw thrust maneuvers.
#### Head Tilt / Chin Lift
Head tilt / chin lift is typically the easier of the two, but if you suspect that someone has suffered neck or spine damage it's better to keep the neck still and use the jaw thrust maneuver. Place one hand on the patient's forehead and tilt their head back while using your other hand to lift their chin up.
#### Jaw Thrust
**If you are not trained in the jaw-thrust maneuver, just stick with head tilt / chin lift. A YouTube video does not count as training.** Here is a [video tutorial](https://youtu.be/dTGCTn5Vnz4?t=40) on how to preform a jaw thrust.
### Rescue Breathing
There are several different methods of rescue breathing. For EMR you are required to know:
- Mouth-to-mask
- Mouth-to-mouth
- Mouth-to-nose
- Mouth-to-stoma

All of these are largely similar and will all be done with two exhalations. For mouth-to-mouth, pinch the patient's nose shut and create an airtight seal with your mouth. Exhale, breathe in through your nose, and exhale again. Mouth-to-mask and mouth-to-stoma are the same except you will be using a mask to cover the face or stoma instead of pinching the nose shut.
{{< figure src="/img/cprmask.jpeg" alt="CPR Mask" position="center" style="border-radius: 8px;" caption="A CPR mask on a training dummy" captionPosition="right" >}}
### Manual Clearance Technqiues
If you see any airway obstruction, try to swab it out with two gloves fingers. If you are not successful, do not try again as you may push the obstruction deeper into the airway.
### Oxygen Therapy
EMR requires knowledge of how to use a nasal cannula and a non-rebreather mask. A nasal cannula is a tube where the end splits into two prongs (one for each nostril) and an air/oxygen mixture is passed through. Usage is as simple as inserting the prongs into each nostril and wrapping the main tube over the patient's ears for support. A non-rebreather mask is a simple mask with straps and is intuitive to put on, but does not allow air from the outside environment to be inhaled so a loss of oxygen supply can be life threatening.
## Cardiovascular / Circulation
### Cardiopulmonary Resuscitation (CPR)
CPR, the thing we all know and love. To preform CPR, apply 30 strong compressions to the sternum at 100-120 compressions per minute, aiming for a depth of ~2 inches. While there can be some damage caused by going too deep, not going deep enough is much more of a problem so make sure you're getting that two inches. After the 30 compressions, preform 2 rescue breaths and resume compressions. A full 30:2 cycle should take about two minutes.
### Automated External Defribilator
They come with visual instructions and have voice prompts telling you what to do. Don't sweat it.
### Hemorrhage Control
Blood is best when it's inside you. That's why EMR requires you learn some very simple skills: applying direct pressure to a wound, using a tourniquet, and wound packing. Applying direct pressure to a wound is pretty self explanatory, but make sure you're wearing gloves because blood has a lot of nasty stuff in it.

That said, applying direct pressure won't help you very much for larget wounds. Press on that gunshot wound through your thigh all you want, your blood is still coming out. For deeper wounds, shove some sterile gauze into it (again wearing gloves, blood is full of diseases you don't want) but not super tight. The gauze will expand as it soaks up blood and if you pack it tightly that expansion will just make the wound bigger.

For massive bleeding for wounds on your arms and legs, you can use a tourniquet to cut off blood flow before it reaches the wound. **DO NOT PUT ONE ON THE NECK. THAT KILLS PEOPLE.** How to apply a tourniquet varies from model to model, but the go-to for most people and organizations is the Combat Application Tourniquet. North American Rescue has a helpful instructions video [on youtube](https://www.youtube.com/watch?v=PMfEls2LC8c). Something important to note, tourniquets hurt a whole lot. Warn the patient of that if they're conscious.
{{< figure src="/img/cat.jpg" alt="Combat Application Tourniquet" position="center" style="border-radius: 8px;" caption="A North American Rescue Combat Application Tourniquet" captionPosition="right" >}}
## Splinting / SMR
### Extremity Splinting
There's a lot of differnet kinds of extremity splinting for different extremities. The Emergency Medicine Residents' Association has a helpful [guide](https://www.emra.org/globalassets/emra/publications/reference-cards/emra_sportsmedicine_splint_guide.pdf) with pictures explaining the different kinds of splinting.

## Medication Administration Routes
For EMR you are only certified to use intramuscular autoinjectors and nasal sprays. That is it. You are not even certified to give anyone inhaled, oral, or topical medications.
## Approved Medications
This one is really easy. EMR doesn't even certify you to give someone OTC pain medications like Tylenol. All you need to know are how to use antidotal and naloxone autoinjectors (don't worry, they come with instructions) and how to narcan someone intranasally. You don't even need to know how to use an epipen.

To use narcan, insert the tip into the patient's right nostril (unless blocked) and depress the plunger. The instructions say to depress the plunger all at once, but if someone is overdosing on opioids bringing them down that quickly is a good way to get punched in the face. Probably go a bit slower than that.
## IV Initiation / Maintenance Fluids
This category is the easiest of them all! EMR (and EMT for that matter) does not certify you for anything intravenous.
## Miscellaneous
You don't really need to know anything here either


## Conclusion
Congratulations, you are now ready (honestly probably very overprepared) to take your EMR exam.