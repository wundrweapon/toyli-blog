+++
title = "Myths About First Aid That Do More Harm Than Good"
date = "2022-07-04"
description = "Please don't do these."
tags = ["medicine"]
+++
Hubris is a bitch, and bad ideas that people convinced themselves were good abound in medicine, especially online. Here's some medical misinformation to be aware of during your time on the internet so you don't end up harming yourself or someone else more than help them.
## Grabbing An Epileptic Person's Tongue So They Don't Swallow It
One alarmingly common belief about seizures is that someone having a seizure might swallow their own tongue. This also takes the form of putting something in a seizure victim's mouth. In reality, the tongue is not a floppy appendage. It's one of the strongest muscles in your body, and physically impossible to swallow. While a seizure victim definitely won't swallow their own tongue, they may very well swallow or choke on whatever you put in their mouth. What you should actually do if someone has a seizure is:
1. Help the person safely to the ground
2. Move away any objects they could hurt themself with
3. Check for pulse / chest rise and fall and preform CPR if needed
4. Call 911
## The Food Myths
### Putting Steak On A Black Eye
I don't actually know where this comes from, but it doesn't work. Some versions of this myth call for specifically a cold steak which will help more, but because of the cold reducing the swelling. You will be better served by a bag of ice, which will make less of a mess and take longer to melt than it will take a cold steak to warm up to room temperature because of the energy required for phase change.
### Pouring Milk In Your Eyes For Tear Gas
Continuing the theme of food-based, a more modern myth that exploded in popularity in the United States during the 2020 George Floyd riots. Another version of this myth is using baking soda or other basic things to neutralize the acidic tear gas.

I never thought I'd have to say this, but please don't try to do acid-base reactions (or really any chemical reaction?) in your eyes. Also, just because milk is good for spice on your tongue doesn't mean that it's good for your eyes. In general please just don't do things to people's eyes, but if you're in a situation where you absolutely must flush the eyes then please follow the example of emergency eye wash stations and just use clean water.
### Rubbing Coffee Grounds Into a Wound To Stop Bleeding
As far as I can tell, this one comes from some parts of Asia. While there are some theories that the vasoconstrictive properties of caffeine may help a little bit, you're much more likely to just end up infecting your wound and making your ER doctor's life harder. They already hate dealing with QuickClot and that actually works. Imagine if you had to clean coffee grounds out of a wound before you could start working on it.
### Putting Butter On Burns
The most important thing to do when someone has just been burned is to remove heat from the affected area as quickly as possible to prevent further damage. This is why your first instinct when you burn your hand is to wave your hand around, using the air moving over your skin to transfer heat away from the burn. The best thing you can do for a burn is to remove any hot clothing or jewellery and run the burn under cold water.

Applying butter (or yogurt, toothpaste, tomato paste, raw egg whites, sliced potato, or any of the other iterations of this myth) does the exact opposite of what you should do to treat a burn. Instead of removing heat from the burn it creates an insulating layer preventing heat from being removed, leading to a more intense burn as the residual heat does more damage before finally diffusing into the body.

This myth and its variants are pervasive and important to try to expel from the public conscious. One [Turkish study](https://pubmed.ncbi.nlm.nih.gov/20211400/) found that, out of 53 burned children, 51% of parents used inappropriate remedies like yogurt. A [British study](https://pubmed.ncbi.nlm.nih.gov/22030439/) found that only 10% of parents understood appropriate burn first aid steps and less than 40% would remove hot clothes.
### Drinking Milk To Cure Poisoning
It just doesn't work. There's not much else to say about it. Real life isn't Minecraft.
## Putting Your Fingers Down Someone's Throat When They're Choking
{{< figure src="/img/badchokingfirstaid.jpg" alt="A cross-section illustration of shoving a whole hand down someone's throat" position="center" style="border-radius: 8px; max-width: 75%; margin-left: auto; margin-right: auto;" caption="First aid guide for choking from 1943" captionPosition="right" captionStyle="width: 75%; margin-left: auto; margin-right: auto;" >}}
The rationale for this one, from what I can tell, is that if you stuff your whole fucking hand down someone's throat, you'll stretch out their throat so they can breathe around the obstruction. The problems with this are:
1. That's not how throats work
2. If it was how throats worked, gravity and your hand entering the throat will just end up moving the obstruction further down
3. Your hand has become a brand new obstruction that can't be breathed around

Please do not do this.
## Slapping A Choking Person's Back
This is another piece of bad choking advice, but now we're getting closer to the adoption of the Heimlich maneuver. While not as bad as trying to fist someone's throat, this is still usually worse than nothing. In the unlikely event it does dislodge the obstruction, whatever was in the person's throat will likely fall further down and increase risk of hospitalization. 
## Cutting A Hole In A Choking Person's Throat
For the love of god do not do this.
## Tilting Your Head Back To Stop Nosebleeds
I don't know where this one comes from either, but don't do it. Tilting your head in either direction does not make the bleeding stop any sooner, but tilting your head back will make blood flow back into your throat and down into your stomach, which will cause vomiting in sufficient volume.
## Putting Frostbitten Limbs In Hot Water
This one was actually a torture method devised by the Japanese [Unit 731](https://en.wikipedia.org/wiki/Unit_731#Frostbite_testing), I don't know how it ever managed to transition from that into being seen by some people as a viable first aid method, but here's what you should actually do if frostbitten:
1. Get out of the cold
2. Remove any wet clothing
3. Hold the frostbitten limb close to your body to reheat it. You _can_ use warm (not hot!) water to rewarm the affected area, but be careful. You generally want the water to be around body temperature.
4. Go to a hospital for anything more than mild frostbite
## Applying Heat To Sprains/Fractures/Swelling
This must have gotten lost in translation when people were told to apply cold to swelling. Applying heat to an area increases the blood flow to that area, which can be comforting or help healing for certain injuries like torn muscles. However, when an injury is swelling, applying heat and increasing the blood flow to the area will only increase the swelling and make the problem worse.
## Ipecac
Ipecac: your scoutmaster's favorite syrup. Approved for OTC sale in 1965 and formerly recommended by several big names in the medical world for first aid in case of accidental poisonings, ipecac syrup is probably the most credible thing in this post. The idea is simple: if you accidentally eat or drink something poisonous, take ipecac syrup quickly to induce vomiting and remove the poisonous thing from your body before you fully absorb it. In practice, however, ipecac isn't very effective. It was discontinued in 2010 and hasn't been manufactured in over a decade now. Despite that, many baby boomers are hanging on to expired containers of this stuff in hopes that one day it will save someone after they've ingested some kind of poison. 
## Popping Blisters
Don't do this. Blisters are your body's way of naturally forming bandages. For some people at risk of infection, especially people with compromised immune systems, a doctor may drain fluid from a blister with a needle, but you should never attempt to do this yourself. By popping a blister you are increasing your risk of infection and slowing down the healing process. Blisters are annoying, but you should just let them do their thing.
