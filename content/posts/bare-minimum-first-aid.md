+++
title = "The Bare Minimum First Aid Kit"
date = "2022-04-20"
author = "Toyli"
description = "It's easy to fall into the trap of trying to be prepared for every possible injury, so where's the line for the bare minimum?"
tags = ["medicine"]
+++
## What Inspired This Post?
In EMS, there's a pervasive problem. When it comes to our medic bags we overthink and try to be prepared for every conceivable injury and, when you've spent a semester or few studying all the different ways a human body can go emergently wrong, the breadth of conceivability is pretty large. I have friends who carry traction splints, junction tourniquets, multi-trauma sheets, intubation kits, etc. The contents of my high-mobility riot bag are worth well over $500. What really inspired me to make this post, though, was this image.

{{< figure src="/img/doterraifak.jpg" alt="A \"First Aid Kit\" of doTerra essential oils" position="center" style="border-radius: 8px; max-width: 75%; margin-left: auto; margin-right: auto;" caption="A \"First Aid Kit\" of doTerra essential oils" captionPosition="right" captionStyle="width: 75%; margin-left: auto; margin-right: auto;" >}}

This is the other end of the spectrum. This is a "first aid kit" made by grifters for people who have no idea what could go wrong beyond their kid in the elementary school soccer team getting a scrape on their elbow. I think it should be a crime to market this as a first aid kit, but that begs the question of where the line to qualify as a "real" first aid kit is. So, what is the bare minimum first aid that can treat any injury that requires immediate first aid without requiring a level of training/specialization that most people can't get?
## Existing Answers
Even though they're not answering exactly the same questions as us, it may be helpful to look at some existing answers of what should be in a first aid kit.
### OSHA's Answer
The OSHA requirement for the contents of a first aid kit is governed by [ANSI Z308.1-1998](https://webstore.ansi.org/Standards/ISEA/ANSIISEAZ3082015) (They want $30 for this list!). It requires:

|Item|Minimum Quantity|
|---|---|
|At least 4x4" Absorbent compress|1|
|1x3" Adhesive Bandages (band-aids)|16|
|5 yards Adhesive Tape|1|
|0.5g Antiseptic packet|10|
|0.5g Burn treatment packet|6|
|Medical Exam Gloves|2 pairs|
|3x3" Sterile pads|4|
|40x40" Triangular Bandage|1|

While a great list of useful tools that are broadly applicable to most situations, these contents won't help very much in certain emergencies and most of them (such as band-aids) are for more for minor injuries.

### North American Rescue's Answer
North American Rescue sells a [Public Access Bleeding Control Kit](https://www.narescue.com/community-preparedness/public-access-kits/public-access-individual-bleeding-control-kit-nylon.html). You've probably even seen one in a wall before. It has a much more specific focus in mind (bleeding control vs a general first aid kit), but we can probably get some useful information out of its contents.

|Item|Quantity|
|---|---|
|Combat Application Tourniquet|1|
|6" Emergency Trauma Dressing|1|
|Wound Packing Gauze|2|
|Nitrile gloves|2 pairs|
|Permanent Marker, small|1|
|Responder trauma shears|1|
|Survival blanket|1|
|First Aid Instructions|1|

While more specific about what it treats than the OSHA kit, this kit includes more emergent supplies than what you'd find in an OSHA first aid kit such as a tourniquet and survival blanket.

### M.A.R.C.H.
The first place I started trying to find the bare minimum first aid kit was the MARCH acronym.
- Massive Hemorrhage
- Airway
- Respirations
- Circulation
- Hypothermia

But this raised two problems immediately, specifically airway and circulation. Giving a random person a laryngoscope is far above any intuitive "bare minimum" and giving them a blood transfusion kit is a multi-thousand dollar bad idea. That said, hemorrhage, respirations, and hyp(o/er)thermia are probably manageable by random people.

## Requirements Of The Bare Minimum First Aid Kit
I think the bare minimum kit should address 4 types of emergencies, as they are maintainable by bystanders until more advanced care arrives.
1. Hemorrhage
2. CPR
3. Hyp(o/er)thermia
4. Drug overdose

### Hemorrhage
This is easily the most complicated part. While it seems intuitive that your blood should be kept inside you, how to best do so depends a lot on where the injury is. Here's a table for reference of what to do where:

|Location|Direct Pressure|Pressure Dressing|Tourniquet|Wound Packing|Occlusive Dressing|
|-|-|-|-|-|-|
|Extremity|✅|✅|✅|✅|❌|
|Junction|✅|❌|❌|✅|❌|
|Chest|Maybe[^1]|❌|❌|❌|✅|
|Abdomen|✅|❌|❌|❌|✅|

Most kits include a tourniquet because they're very good at stopping blood flow, but they only work effectively on extremities. Gauze for packing junction wounds and an occlusive dressing for chest and abdomen wounds should also be added to the kit.

### CPR
While using anything in this section will require additional education (such as a BLS or CPR class), the classes are widespread and generally inexpensive. Many employers will even pay for you to take a BLS class. While CPR doesn't strictly _require_ a mask, they exist for a reason and I will be considering it part of the minimum.

### Hyp(o/er)thermia
Hypothermia, even in warm climates, is a common symptom of shock when someone is severely injured and can be life-threatening. You should carry a Mylar blanket in your bare minimum kit and, if possible, have someone else inside the blanket with the injured person to act as a heat generator.

Hyperthermia, on the other hand, is possible even in cold climates and is caused by a variety of things such as overexertion or serotonin syndrome from drug use. A first aid kit should include at least one, ideally two, instant cold packs to place inside the armpit of a hyperthermic person.

### Drug Overdose
The final requirement of a first aid kit is Narcan. Opioid overdose has killed more Americans than the Iraq, Afghanistan, and Vietnam wars combined. This kit + BLS training already contains equipment to manage symptoms of various drug overdoses such as respiratory arrest, hypothermia, and hyperthermia. Narcan is often available for free and can temporarily reverse the effects of an opioid overdose, which is critical for keeping someone alive. 

## Contents
With all of that said, I believe the following is the bare minimum first aid kit:

1. Nitrile Gloves (Other peoples' blood has all sorts of nasty diseases in it you don't want)
2. A [tourniquet](https://www.narescue.com/combat-application-tourniquet-c-a-t-tb.html)
3. [Wound packing gauze](https://www.narescue.com/nar-wound-packing-gauze.html)
4. [Occlusive dressing](https://www.narescue.com/hyfin-vent-chest-seal.html)
5. CPR Mouthpiece
6. [Mylar Blanket](https://www.narescue.com/nar-survival-blanket.html)
7. [Instant cold pack(s)](https://mms.mckesson.com/product/476730/McKesson-Brand-16-9701)
8. Narcan

## Recommended Additions
Above is what I consider the bare minimum first aid kit. Your first aid kit probably _should_ contain more than this. This section contains some extras I would suggest adding and what situations they might be especially important or even essential in.
### Splint
The North American Rescue [SAM Splint II](https://www.narescue.com/sam-splint-ii.html) is a versatile splint that, combined with some tape[^2] can be molded to support almost all kinds of fracture. Properly splinting a fracture quickly can improve outcomes and help ensure the fracture heals correctly, especially if you're somewhere where emergency services will not arrive quickly.

### Adhesive Bandages
Just because adhesive bandages aren't typically for life threatening circumstances like the 8 items listed above doesn't mean they aren't useful. Any off the shelf first aid kit will have some and there's a reason for that. Get a tube of antibiotic ointment to go with them to make sure cuts and scrapes don't get infected.

### Hand Sanitizer
The main kit includes gloves to keep blood off of your hands, but some hand sanitizer is highly effective in preventing spread of pathogens across multiple casualties ***if your hands are not soiled***. Hand sanitizer will only kill germs, not clean something off of your hands. Get some single use [packets](https://www.amazon.com/Advanced-Sanitizer-Fragrance-Portable-Individual/dp/B01G1KP8C2/) or [towlettes](https://www.amazon.com/Sanitizing-Alcohol-Fragrance-Individually-Portable/dp/B014EVXZ9S/) to keep in your kit and clean your hands before and after taking off your gloves.

### Burn Gel / Dressings
Thermal burns can be very common in kitchens and some industrial settings. For this, you can get some general purpose burn gel or [BurnTec Dressings](https://www.narescue.com/burntec-dressing.html) for larger burn areas. Personally I have some 4x4" ones, but they come in many sizes.

### Serrated Adson Forceps
Adson forceps are great for pulling out splinters, cactus spines, and other small foreign bodies embedded in the skin. You can find adson forceps and other surgical instruments [here](https://www.narescue.com/nar-surgical-instruments.html).

### Afro Pick
If, like me, you're from somewhere with [jumping cholla](https://en.wikipedia.org/wiki/Cylindropuntia_fulgida), I consider an afro pick to be a must-have if you're ever going out into the desert. You can find afro picks at most beauty stores, but try to find a black-owned store if you want one you can depend on for repeated use.

[^1]: It is important to not apply too much force to the chest during an open injury. While it is better to use an occlusive dressing for open chest injuries to prevent buildup of air in the pleural cavity, which is supposed to be filled with fluid, applying direct pressure is better than nothing if an occlusive dressing is not available. Applying pressure to an entry/exit wound is not recommended.
[^2]: Medical cloth tape is usually used, but even just duct tape is fine. Anything that will reliably attach the splint to the fracture.